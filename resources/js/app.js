import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";


Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);

import App from './View/App'
import Login from './Pages/login'
import Home from './Pages/home'
import ChangePass from './Pages/changepassword'
import Companies from './Company/companies'
import Clients from './Clients/clients'
import Branches from './Branches/branches'
import Guards from './Guards/guards'
import Edit_Client from './Clients/edit_client'
import Edit_Branch from './Branches/edit_branch'
import Report from './Report/company_report'
import Edit_company from './Company/edit_company'
import Users from './User/users'
import Profile from './Pages/profile'
import Forget from './Pages/forget'
import Confirm from './Pages/confirm'





const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/password/forget',
            name: 'forget',
            component: Forget,
            meta: { hideNavigation: true }
        },
        {
            path: '/password/confirm',
            name: 'confirm',
            component: Confirm,
            meta: { hideNavigation: true }
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/companies',
            name: 'companies',
            component: Companies
        },

        {
            path: '/password/change',
            name: 'changepassword',
            component: ChangePass,
            meta: { hideNavigation: true }
        },
        {
            path: '/clients',
            name: 'clients',
            component: Clients
        },
        {
            path: '/branches',
            name: 'branches',
            component: Branches
        },
        {
            path: '/guards',
            name: 'guards',
            component: Guards
        },
        {
            path: '/client/edit/:id',
            name: 'get_cleint_details',
            component: Edit_Client
        },
        {
            path: '/branch/edit/:id',
            name: 'edit_branch',
            component: Edit_Branch
        },
        {
            path: '/reports',
            name: 'reports',
            component: Report
        },
        {
            path: '/company/edit/:id',
            name: 'edit_company',
            component: Edit_company
        },
        {
            path: '/users',
            name: 'users',
            component: Users
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },

    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});